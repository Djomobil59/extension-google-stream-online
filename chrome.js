var djomobil_nav = {
    doNotification: function() {
        chrome.notifications.clear('notifyON' + stream_params.title, function(id) { });
        chrome.notifications.create('notifyON' + stream_params.title, { type: "basic", title: stream_params.title, message: stream_params.message, iconUrl: "iconon128.png" }, function(id) { });
    },
    setIconON: function(on) {
        var status = on ? "on" : "off";
        chrome.browserAction.setIcon({path : "icon" + status + "48.png"});
    },
    goIt: function() {
        if(zerator.isON){
            chrome.tabs.create({url:zerator.getCurrentRedirectUrl()},function(tab){});
        } else {
            chrome.tabs.create({url:stream_params.offlineUrl},function(tab){});
        }
    }
}

chrome.browserAction.onClicked.addListener(djomobil_nav.goIt);
chrome.notifications.onClicked.addListener(function(notificationId){
    if (notificationId === 'notifyON' + stream_params.title) {
        chrome.tabs.create({url:zerator.getCurrentRedirectUrl()},function(tab){});
    }
});

djomobil_nav.setIconON(false);
var zerator = new BtnLive(stream_params.chaines, function(result) {
    djomobil_nav.setIconON(result);
    if (result) {
        djomobil_nav.doNotification();
    }
}, 60000, 2);
